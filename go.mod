module bitbucket.org/ahmedaabouzied/ah_ab

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/sendgrid/rest v2.6.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.6.2+incompatible
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
)
