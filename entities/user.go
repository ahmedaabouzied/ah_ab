package entities

// User represents the user entity.
type User struct {
	ID             int64  `json:"id"`
	Email          string `json:"email"`
	FullName       string `json:"fullName"`
	Telephone      string `json:"telephone"`
	Address        string `json:"address"`
	HashedPassword []byte `json:"hashedPassword"`
}
