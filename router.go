package main

import (
	_userapi "bitbucket.org/ahmedaabouzied/ah_ab/user/api"
	_userrepo "bitbucket.org/ahmedaabouzied/ah_ab/user/repository"
	_userusecase "bitbucket.org/ahmedaabouzied/ah_ab/user/usecase"
	"database/sql"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

func startServer(db *sql.DB) {
	port := os.Getenv("PORT")

	userRepo := _userrepo.NewUserRepository(db)
	userUsecase := _userusecase.NewUserUsecase(userRepo)
	userAPI := _userapi.NewUserAPI(userUsecase)
	router := mux.NewRouter()
	router.HandleFunc("/api/user/password", userAPI.SignUpWithPassword).Methods("POST")
	router.HandleFunc("/api/login/password", userAPI.LoginWithPassword).Methods("POST")
	router.HandleFunc("/api/login/token", userAPI.LoginWithGoogle).Methods("POST")
	router.HandleFunc("/api/user/token", userAPI.SignUpWithGoogle).Methods("POST")
	router.HandleFunc("/api/forget-pass", userAPI.ForgetPassword).Methods("POST")
	router.HandleFunc("/api/reset-pass", userAPI.ResetPassword).Methods("POST")
	router.HandleFunc("/api/user", userAPI.GetUser).Methods("GET")
	router.HandleFunc("/api/user", userAPI.UpdateUser).Methods("PUT")
	log.Println("Starting HTTP server")
	http.ListenAndServe(fmt.Sprintf(":%s", port), router)
}
