package user

import (
	"bitbucket.org/ahmedaabouzied/ah_ab/entities"
	"context"
)

// Usecase has the usecase methods for the user
type Usecase interface {
	SignUpWithPassword(ctx context.Context, user *entities.User, password string) (string, *entities.User, error)
	SignUpWithGoogle(ctx context.Context, token string) (string, *entities.User, error)
	LoginWithPassword(ctx context.Context, email string, password string) (string, *entities.User, error)
	LoginWithGoogle(ctx context.Context, token string) (string, *entities.User, error)
	GetUserByID(ctx context.Context, ID int64) (*entities.User, error)
	UpdateUser(ctx context.Context, user *entities.User) (*entities.User, error)
	ForgetPassword(ctx context.Context, userEmail string) error
	ResetPassword(ctx context.Context, token string, password string) error
}
