package repository

import (
	"bitbucket.org/ahmedaabouzied/ah_ab/entities"
	"bitbucket.org/ahmedaabouzied/ah_ab/user"
	"context"
	"database/sql"
)

// UserRepository implement DB CRUD methods for Users
type UserRepository struct {
	db *sql.DB
}

func NewUserRepository(db *sql.DB) user.Repository {
	return &UserRepository{db}
}

// CreateUser creates a new User database record
func (r *UserRepository) CreateUser(ctx context.Context, user *entities.User) (*entities.User, error) {
	ctx, cancelFunc := context.WithCancel(ctx)
	result, err := r.db.ExecContext(ctx, `INSERT INTO Users (
            Email,
            FullName,
            Address,
            Telephone,
            HashedPassword
        ) VALUES (?,?,?, ?, ?);
    `, user.Email, user.FullName, user.Address, user.Telephone, user.HashedPassword)
	if err != nil {
		cancelFunc()
		return nil, err
	}
	ID, err := result.LastInsertId()
	if err != nil {
		cancelFunc()
		return nil, err
	}
	user.ID = ID
	cancelFunc()
	return user, nil
}

// GetGetByID returns the user with the given ID.
func (r *UserRepository) GetByID(ctx context.Context, ID int64) (*entities.User, error) {
	ctx, cancelFunc := context.WithCancel(ctx)
	var user entities.User
	err := r.db.QueryRowContext(ctx, `SELECT u.ID, u.Email,u.FullName, u.Address,u.Telephone, u.HashedPassword FROM Users AS u WHERE u.ID = ?;`, ID).Scan(&user.ID, &user.Email, &user.FullName, &user.Address, &user.Telephone, &user.HashedPassword)
	if err != nil {
		cancelFunc()
		return nil, err
	}
	cancelFunc()
	return &user, nil
}

// GetGetByEmail returns the user with the given email.
func (r *UserRepository) GetByEmail(ctx context.Context, email string) (*entities.User, error) {
	ctx, cancelFunc := context.WithCancel(ctx)
	var user entities.User
	err := r.db.QueryRowContext(ctx, `SELECT u.ID, u.Email,u.FullName, u.Address,u.Telephone, u.HashedPassword FROM Users AS u WHERE u.Email= ?;`, email).Scan(&user.ID, &user.Email, &user.FullName, &user.Address, &user.Telephone, &user.HashedPassword)
	if err != nil {
		cancelFunc()
		return nil, err
	}
	cancelFunc()
	return &user, nil
}

func (r *UserRepository) UpdateUser(ctx context.Context, user *entities.User, ID int64) (*entities.User, error) {
	ctx, cancelFunc := context.WithCancel(ctx)
	_, err := r.db.ExecContext(ctx, `UPDATE Users SET Email = ?, FullName = ?, Address = ?, Telephone = ? WHERE ID = ?;`, user.Email, user.FullName, user.Address, user.Telephone, ID)
	if err != nil {
		cancelFunc()
		return nil, err
	}
	if err != nil {
		cancelFunc()
		return nil, err
	}
	cancelFunc()
	return user, nil
}

func (r *UserRepository) UpdatePassword(ctx context.Context, ID int64, hash []byte) error {
	ctx, cancelFunc := context.WithCancel(ctx)
	_, err := r.db.ExecContext(ctx, `UPDATE Users SET HashedPassword = ? WHERE ID = ?;`, hash, ID)
	if err != nil {
		cancelFunc()
		return err
	}
	if err != nil {
		cancelFunc()
		return err
	}
	cancelFunc()
	return nil
}
