package usecase

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"bitbucket.org/ahmedaabouzied/ah_ab/auth"
	"bitbucket.org/ahmedaabouzied/ah_ab/email"
	"bitbucket.org/ahmedaabouzied/ah_ab/entities"
	"bitbucket.org/ahmedaabouzied/ah_ab/user"
	"golang.org/x/crypto/bcrypt"
)

type GoogleProfile struct {
	Email string `json:"email"`
	Name  string `json:"name"`
}

type UserUsecase struct {
	userRepo user.Repository
}

func NewUserUsecase(userRepo user.Repository) user.Usecase {
	return &UserUsecase{userRepo}
}

// SiSignUpWithPassword creates a new user with email and password
func (u *UserUsecase) SignUpWithPassword(ctx context.Context, user *entities.User, password string) (string, *entities.User, error) {
	ctx, cancelFunc := context.WithCancel(ctx)
	// Hash password
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		cancelFunc()
		return "", nil, err
	}
	user.HashedPassword = hash
	// Save user
	createdUser, err := u.userRepo.CreateUser(ctx, user)
	if err != nil {
		cancelFunc()
		return "", nil, err
	}
	// Generate JWT token
	jwtToken, err := auth.GenerateAuthToken(user.ID)
	if err != nil {
		cancelFunc()
		return "", nil, err
	}
	cancelFunc()
	return jwtToken, createdUser, nil
}

func (u *UserUsecase) SignUpWithGoogle(ctx context.Context, token string) (string, *entities.User, error) {
	ctx, cancelFunc := context.WithCancel(ctx)
	// Get profile from Google
	profile, err := getGoogleProfile(token)
	if err != nil {
		cancelFunc()
		return "", nil, err
	}
	// Save user
	createdUser, err := u.userRepo.CreateUser(ctx, &entities.User{
		Email:    profile.Email,
		FullName: profile.Name,
	})
	if err != nil {
		cancelFunc()
		return "", nil, err
	}
	// Generate JWT token
	jwtToken, err := auth.GenerateAuthToken(createdUser.ID)
	if err != nil {
		cancelFunc()
		return "", nil, err
	}
	cancelFunc()
	return jwtToken, createdUser, nil
}

func (u *UserUsecase) LoginWithPassword(ctx context.Context, email string, password string) (string, *entities.User, error) {
	ctx, cancelFunc := context.WithCancel(ctx)
	user, err := u.userRepo.GetByEmail(ctx, email)
	if err != nil {
		cancelFunc()
		return "", nil, errors.New("not found")
	}
	err = bcrypt.CompareHashAndPassword(user.HashedPassword, []byte(password))
	if err != nil {
		cancelFunc()
		return "", nil, errors.New("wrong password")
	}
	// Generate JWT token
	jwtToken, err := auth.GenerateAuthToken(user.ID)
	if err != nil {
		cancelFunc()
		return "", nil, err
	}
	cancelFunc()
	return jwtToken, user, nil
}

func (u *UserUsecase) GetUserByID(ctx context.Context, ID int64) (*entities.User, error) {
	ctx, cancelFunc := context.WithCancel(ctx)
	user, err := u.userRepo.GetByID(ctx, ID)
	if err != nil {
		cancelFunc()
		return nil, errors.New("not found")
	}
	cancelFunc()
	return user, nil
}

func (u *UserUsecase) LoginWithGoogle(ctx context.Context, token string) (string, *entities.User, error) {
	ctx, cancelFunc := context.WithCancel(ctx)
	// Get profile from Google
	profile, err := getGoogleProfile(token)
	if err != nil {
		cancelFunc()
		return "", nil, err
	}
	user, err := u.userRepo.GetByEmail(ctx, profile.Email)
	if err != nil {
		cancelFunc()
		return "", nil, errors.New("not found")
	}
	// Generate JWT token
	jwtToken, err := auth.GenerateAuthToken(user.ID)
	if err != nil {
		cancelFunc()
		return "", nil, err
	}
	cancelFunc()
	return jwtToken, user, nil
}

// UpdateUser updates details of the given user
func (u *UserUsecase) UpdateUser(ctx context.Context, user *entities.User) (*entities.User, error) {
	ctx, cancelFunc := context.WithCancel(ctx)
	updatedUser, err := u.userRepo.UpdateUser(ctx, user, user.ID)
	if err != nil {
		cancelFunc()
		return nil, err
	}
	cancelFunc()
	return updatedUser, nil
}

func (u *UserUsecase) ForgetPassword(ctx context.Context, userEmail string) error {
	ctx, cancelFunc := context.WithCancel(ctx)
	user, err := u.userRepo.GetByEmail(ctx, userEmail)
	if err != nil {
		cancelFunc()
		return err
	}
	token, err := auth.GenerateResetPassToken(user.ID)
	if err != nil {
		cancelFunc()
		return err
	}
	err = email.SendResetPasswordEmail(user.Email, user.FullName, token)
	if err != nil {
		cancelFunc()
		return err
	}
	cancelFunc()
	return nil
}

func (u *UserUsecase) ResetPassword(ctx context.Context, token string, password string) error {
	ctx, cancelFunc := context.WithCancel(ctx)
	ID, err := auth.ParseToken(token)
	if err != nil {
		cancelFunc()
		return err
	}
	user, err := u.userRepo.GetByID(ctx, ID)
	if err != nil {
		cancelFunc()
		return err
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		cancelFunc()
		return err
	}
	err = u.userRepo.UpdatePassword(ctx, user.ID, hash)
	if err != nil {
		cancelFunc()
		return err
	}
	cancelFunc()
	return nil
}

func getGoogleProfile(token string) (*GoogleProfile, error) {
	resp, err := http.Get(fmt.Sprintf("https://oauth2.googleapis.com/tokeninfo?id_token=%s", token))
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, errors.New("error getting google profile")
	}
	defer resp.Body.Close()
	var googleProfile GoogleProfile
	err = json.NewDecoder(resp.Body).Decode(&googleProfile)
	if err != nil {
		return nil, err
	}
	return &googleProfile, nil
}
