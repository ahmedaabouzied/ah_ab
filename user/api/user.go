package api

import (
	"context"
	"encoding/json"
	"net/http"

	"bitbucket.org/ahmedaabouzied/ah_ab/auth"
	"bitbucket.org/ahmedaabouzied/ah_ab/entities"
	"bitbucket.org/ahmedaabouzied/ah_ab/user"
)

type UserAPI struct {
	userUsecase user.Usecase
}

type errorReponse struct {
	Error   string `json:"error"`
	Message string `json:"message"`
}

type okUserResponse struct {
	User  entities.User `json:"user"`
	Token string        `json:"token,omitempty"`
}

type newUserWithPassword struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type newUserWithToken struct {
	Token string `json:"token"`
}

type forgetPasswordReq struct {
	Email string `json:"email"`
}

type resetPasswordReq struct {
	Password string `json:"password"`
}

func NewUserAPI(usecase user.Usecase) *UserAPI {
	return &UserAPI{userUsecase: usecase}
}

func (h *UserAPI) SignUpWithPassword(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	defer r.Body.Close()
	var reqBody newUserWithPassword
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		sendError(w, err, "error parsing json body of request")
	}
	userToCreate := entities.User{
		Email: reqBody.Email,
	}
	token, createdUser, err := h.userUsecase.SignUpWithPassword(ctx, &userToCreate, reqBody.Password)
	if err != nil {
		sendError(w, err, "error creating user")
		return
	}
	sendCreated(w, createdUser, token)
	return
}

func (h *UserAPI) SignUpWithGoogle(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	defer r.Body.Close()
	var reqBody newUserWithToken
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		sendError(w, err, "error parsing json body of request")
	}
	token, createdUser, err := h.userUsecase.SignUpWithGoogle(ctx, reqBody.Token)
	if err != nil {
		sendError(w, err, "error creating user")
		return
	}
	sendCreated(w, createdUser, token)
	return
}

func (h *UserAPI) LoginWithPassword(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	defer r.Body.Close()
	var reqBody newUserWithPassword
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		sendError(w, err, "error parsing json body of request")
	}
	token, user, err := h.userUsecase.LoginWithPassword(ctx, reqBody.Email, reqBody.Password)
	if err != nil {
		if err.Error() == "not found" {
			sendError(w, err, "user not registered")
			return
		}
		if err.Error() == "wrong password" {
			sendError(w, err, "wrong password")
			return
		}
		sendError(w, err, "error getting user")
		return
	}
	sendOk(w, user, token)
	return
}

func (h *UserAPI) LoginWithGoogle(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	defer r.Body.Close()
	var reqBody newUserWithToken
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		sendError(w, err, "error parsing json body of request")
	}
	token, user, err := h.userUsecase.LoginWithGoogle(ctx, reqBody.Token)
	if err != nil {
		sendError(w, err, "error getting user")
		return
	}
	sendOk(w, user, token)
	return
}

func (h *UserAPI) GetUser(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	defer r.Body.Close()
	token := r.Header.Get("Authorization")
	if token == "" {
		sendUnAuthroized(w)
		return
	}
	ID, err := auth.ParseToken(token)
	if err != nil {
		sendUnAuthroized(w)
		return
	}
	user, err := h.userUsecase.GetUserByID(ctx, ID)
	if err != nil {
		sendError(w, err, "error getting user")
		return
	}
	sendOk(w, user, "")
	return
}

func (h *UserAPI) UpdateUser(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	defer r.Body.Close()
	token := r.Header.Get("Authorization")
	if token == "" {
		sendUnAuthroized(w)
		return
	}
	ID, err := auth.ParseToken(token)
	if err != nil {
		sendUnAuthroized(w)
		return
	}
	var reqBody entities.User
	err = json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		sendError(w, err, "error parsing json body of request")
	}
	reqBody.ID = ID
	updatedUser, err := h.userUsecase.UpdateUser(ctx, &reqBody)
	if err != nil {
		sendError(w, err, "error creating user")
		return
	}
	sendOk(w, updatedUser, "")
	return
}

func (h *UserAPI) ForgetPassword(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	defer r.Body.Close()
	var reqBody forgetPasswordReq
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		sendError(w, err, "error parsing json body of request")
		return
	}
	err = h.userUsecase.ForgetPassword(ctx, reqBody.Email)
	if err != nil {
		sendError(w, err, "user not found")
		return
	}
	w.WriteHeader(200)
	w.Write([]byte("{\"message\":\"reset password email sent\"}"))
	return
}

func (h *UserAPI) ResetPassword(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	defer r.Body.Close()
	token := r.Header.Get("Authorization")
	if token == "" {
		sendUnAuthroized(w)
		return
	}
	var reqBody resetPasswordReq
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		sendError(w, err, "error parsing json body of request")
		return
	}
	err = h.userUsecase.ResetPassword(ctx, token, reqBody.Password)
	if err != nil {
		sendError(w, err, "user not found")
		return
	}
	w.WriteHeader(200)
	w.Write([]byte("{\"message\":\"Password updated successfully\"}"))
	return
}

func sendError(w http.ResponseWriter, err error, msg string) {
	errRes := errorReponse{
		Error:   err.Error(),
		Message: msg,
	}
	body, err := json.Marshal(errRes)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
	w.WriteHeader(http.StatusBadRequest)
	w.Write(body)
}

func sendUnAuthroized(w http.ResponseWriter) {
	errRes := errorReponse{
		Error:   "unauthorized",
		Message: "user is not signed in",
	}
	body, err := json.Marshal(errRes)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
	w.WriteHeader(http.StatusUnauthorized)
	w.Write(body)
}

func sendCreated(w http.ResponseWriter, user *entities.User, token string) {
	resBody := okUserResponse{
		User:  *user,
		Token: token,
	}
	body, err := json.Marshal(resBody)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
	w.WriteHeader(http.StatusCreated)
	w.Write(body)
}

func sendOk(w http.ResponseWriter, user *entities.User, token string) {
	resBody := okUserResponse{
		User: *user,
	}
	if token != "" {
		resBody.Token = token
	}
	body, err := json.Marshal(resBody)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
	w.WriteHeader(http.StatusOK)
	w.Write(body)
}
