package user

import (
	"bitbucket.org/ahmedaabouzied/ah_ab/entities"
	"context"
)

// Repository has the methods to store user data
type Repository interface {
	GetByID(ctx context.Context, ID int64) (*entities.User, error)
	GetByEmail(ctx context.Context, email string) (*entities.User, error)
	CreateUser(ctx context.Context, user *entities.User) (*entities.User, error)
	UpdateUser(ctx context.Context, user *entities.User, ID int64) (*entities.User, error)
	UpdatePassword(ctx context.Context, ID int64, hash []byte) error
}
