package main

import (
	"database/sql"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	port := os.Getenv("PORT")

	log.Println("Starting application")

	pool, err := connectToDB()
	if err != nil {
		log.Fatalln(err.Error())
	}
	err = setupTables(pool)
	if err != nil {
		log.Fatalln(err.Error())
	}
	startServer(pool)
}

func connectToDB() (*sql.DB, error) {
	pool, err := sql.Open("mysql", os.Getenv("DB_CONNECTION_STRING"))
	if err != nil {
		return nil, err
	}
	pool.SetConnMaxLifetime(time.Minute * 3)
	pool.SetMaxOpenConns(10)
	pool.SetMaxIdleConns(10)
	return pool, nil
}

func setupTables(pool *sql.DB) error {
	_, err := pool.Query(
		`CREATE TABLE IF NOT EXISTS Users (
        ID int NOT NULL PRIMARY KEY AUTO_INCREMENT,
        Email VARCHAR(255) UNIQUE,
        FullName VARCHAR(255),
        Address TEXT,
        Telephone VARCHAR(20),
        HashedPassword CHAR(60)
    );`)
	if err != nil {
		return err
	}
	return nil
}
