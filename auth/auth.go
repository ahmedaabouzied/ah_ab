package auth

import (
	"errors"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var jwtKey []byte

// Claims defines JWT claims for the id value
type Claims struct {
	ID int64 `json:"id"`
	jwt.StandardClaims
}

// generateAuthToken generates a login token with JWT
// The login token expires in 7 days.
func GenerateAuthToken(id int64) (string, error) {
	jwtKey = []byte(os.Getenv("JWT_SECRET_KEY"))

	expirationTime := time.Now().Add(7 * 24 * 60 * time.Minute)
	claim := &Claims{
		ID: id,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

// GenerateResetPassToken generates a token with JWT
// The login token expires in 15 minutes.
func GenerateResetPassToken(id int64) (string, error) {
	jwtKey = []byte(os.Getenv("JWT_SECRET_KEY"))

	expirationTime := time.Now().Add(15 * time.Minute)
	claim := &Claims{
		ID: id,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

// parseToken gets the user ID out the token
func ParseToken(tokenString string) (int64, error) {
	token, err := jwt.ParseWithClaims(tokenString, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("JWT_SECRET_KEY")), nil
	})
	if err != nil {
		return 0, err
	}
	claims, ok := token.Claims.(*Claims)
	if !ok {
		return 0, errors.New("could not retrieve id from token")
	}
	return claims.ID, nil
}
