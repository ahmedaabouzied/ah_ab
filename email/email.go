package email

import (
	"errors"
	"fmt"
	"os"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

func SendResetPasswordEmail(email string, name string, token string) error {
	link := fmt.Sprintf("%s/reset-password?token=%s", os.Getenv("ROOT_URL"), token)
	from := mail.NewEmail("AH_AB Application", "noreply@ah_ab.com")
	subject := "Reset password"
	to := mail.NewEmail(name, email)
	plainTextContent := fmt.Sprintf("Reset your password using the following link \n %s \n", link)
	htmlContent := fmt.Sprintf("<strong>%s</strong>", plainTextContent)
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(os.Getenv("SENDGRID_API_KEY"))
	response, err := client.Send(message)
	if err != nil {
		return err
	}
	if response.StatusCode != 202 {
		return errors.New("error sending email")
	}
	return nil
}
